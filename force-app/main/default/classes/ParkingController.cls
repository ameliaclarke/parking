public without sharing class ParkingController {
    public static final String USER_PARAMETER = 'u';

    public List<ParkRequest__c> todaysParkers {get; set;}

    public List<ParkingService.ParkRequestWrapper> parkRequestWrappers {get; set;}

    public String usersName {get; set;}
    public Boolean superParkAvailable {
        get{
            for (ParkingService.ParkRequestWrapper prw : this.parkRequestWrappers) {
                if (prw.parkRequest.SuperPark__c && prw.parkRequest.AllocationCompleted__c) {
                    return false;
                }
            }
            return true;
        }
    }

    public Boolean spareParkAvailable {
        get{ return this.parkingSetting.NumberOfSpaces__c > this.todaysParkers.size(); }
    }

    public Date parkingDate {get; set;}
    public Date parkingRequestDates {get; set;}

    private Contact contact;

    private ParkingSetting__mdt parkingSetting {
        get {
            if(this.parkingSetting == null) {
                this.parkingSetting = [SELECT AllocationTime__c, NumberOfSpaces__c, DayToAllocate__c FROM ParkingSetting__mdt WHERE DeveloperName = 'Auckland' LIMIT 1];
            }
            return parkingSetting;
        }
        set;
    }

    public ParkingController() {
        this.parkingDate = getParkingDate();
        this.parkingRequestDates = Date.today();

        this.todaysParkers = getTodaysParkRecords();

        String usernameHash = ApexPages.currentPage().getParameters().get(USER_PARAMETER);
        List<Contact> contacts = [SELECT Id, FirstName FROM Contact WHERE Hash__c = :usernameHash];
        if (String.isBlank(usernameHash) || contacts.isEmpty()) {
            // ApexPages.addMessage(
            //     new ApexPages.Message(ApexPages.Severity.ERROR,'We couldnt find your user, make user your URL is correct.')
            // );
        } else {
            this.contact = contacts[0];
            this.usersName = this.contact.FirstName;
            this.parkRequestWrappers = ParkingService.getContactsParkingRequestRecords(this.contact.Id, this.parkingRequestDates);
        }
    }

    public void save() {
        ParkingService.saveContactsParkingRequestRecords(this.parkRequestWrappers);
    }

    public void claimPark() {
        ParkingService.updatePark(this.contact.Id, this.parkingDate, true);
        this.todaysParkers = getTodaysParkRecords();
    }

    public void releasePark() {
        ParkingService.updatePark(this.contact.Id, this.parkingDate, false);
        this.todaysParkers = getTodaysParkRecords();
    }

    public void updateTodaysParkRecords() {
        this.todaysParkers = getTodaysParkRecords();
    }

    public void previousWeek() {
        this.parkingRequestDates = this.parkingRequestDates.addDays(-7);
        this.parkRequestWrappers = ParkingService.getContactsParkingRequestRecords(this.contact.Id, this.parkingRequestDates);
    }

    public void nextWeek() {
        this.parkingRequestDates = this.parkingRequestDates.addDays(7);
        this.parkRequestWrappers = ParkingService.getContactsParkingRequestRecords(this.contact.Id, this.parkingRequestDates);
    }

    // gets the date that we care about for parking
    // depends on when the allocation was and what day it was for
    private Date getParkingDate() {
        Boolean todaysAllocationHasHappened = DateTime.now().time() > this.parkingSetting.AllocationTime__c.time();
        if (this.parkingSetting.DayToAllocate__c == 'Today') {
            return todaysAllocationHasHappened ? Date.today() : Date.today().addDays(-1);
        } else {
            return todaysAllocationHasHappened ? Date.today().addDays(1) : Date.today();
        }
    }

    private List<ParkRequest__c> getTodaysParkRecords(){
        List<ParkRequest__c> todaysParkersRecords = [
            SELECT Id,
                     Name,
                     Date__c,
                     ParkAllocated__c,
                     Requested__c,
                     SuperPark__c,
                     Contact__c,
                     Contact__r.Name,
                     Contact__r.Hash__c
            FROM ParkRequest__c
            WHERE ParkAllocated__c = true
                AND Date__c = :this.parkingDate
        ];
        return todaysParkersRecords;
    }

}