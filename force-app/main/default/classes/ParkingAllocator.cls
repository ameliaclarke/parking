public without sharing class ParkingAllocator implements Schedulable {

    public static void schedule() {
        ParkingSetting__mdt aucklandSetting = getParkingSettingMetadata();

        String jobName = aucklandSetting.Label + ' Parking Allocation';
        String sch = String.format(
            '0 {0} {1} ? * *',
            new List<Object>{
                aucklandSetting.AllocationTime__c.minute(),
                aucklandSetting.AllocationTime__c.hour()
            }
        );
        String jobID = System.schedule(jobName, sch, new ParkingAllocator());
    }

    public void execute(SchedulableContext SC) {
        ParkingSetting__mdt aucklandSetting = getParkingSettingMetadata();

        Date parkingDate = Date.today();
        if (aucklandSetting.DayToAllocate__c == 'Tomorrow') {
            parkingDate = Date.today().addDays(1);
        }

        Integer numberOfSpaces = Integer.valueOf(aucklandSetting.NumberOfSpaces__c);

        determineParkers(parkingDate, numberOfSpaces);
    }


    public static void determineParkers(Date parkingDate) {
        ParkingSetting__mdt aucklandSetting = getParkingSettingMetadata();
        Integer numberOfSpaces = Integer.valueOf(aucklandSetting.NumberOfSpaces__c);

        determineParkers(parkingDate, numberOfSpaces);
    }

    public static void determineParkers(Date parkingDate, Integer numberOfSpaces) {
        Integer availableSpaces = numberOfSpaces;

        Integer allocatedSpaces = [
            SELECT count()
            FROM ParkRequest__c
            WHERE ParkAllocated__c = true
                AND Date__c = :parkingDate
        ];
        availableSpaces -= allocatedSpaces;

        List<ParkRequest__c> parkRequests = [
            SELECT Id, Name, Date__c, ParkAllocated__c, Requested__c, SuperPark__c, Contact__c
            FROM ParkRequest__c
            WHERE Requested__c = true
                AND Date__c = :parkingDate
                AND ParkAllocated__c = false
        ];

        List<Id> requestedContactIds = new List<Id>();
        for (ParkRequest__c pr : parkRequests) {
            requestedContactIds.add(pr.Contact__c);
        }

        Map<Id, Contact> contactsRecentParks = new Map<Id, Contact>([
            SELECT Id,
                (
                    SELECT Id, Contact__c
                    FROM ParkRequests__r
                    WHERE ParkAllocated__c = true
                        AND Date__c >= :parkingDate.addDays(-14)
                        AND Date__c < :parkingDate
                )
            FROM Contact
            WHERE Id IN :requestedContactIds
        ]);

        List<ParkRequestWrapper> wrappers = new List<ParkRequestWrapper>();
        for (ParkRequest__c pr : parkRequests) {
            Integer numberOfRecentParks = 0;
            if(contactsRecentParks.containsKey(pr.Contact__c)) {
                numberOfRecentParks = contactsRecentParks.get(pr.Contact__c).ParkRequests__r.size();
            }
            wrappers.add(new ParkRequestWrapper(pr, numberOfRecentParks));
        }

        wrappers.sort();

        List<ParkRequest__c> updatedParkingRequests = new List<ParkRequest__c>();
        for (Integer i=0; i<wrappers.size(); i++) {
            ParkRequest__c prToUpdate = new ParkRequest__c(
                Id = wrappers[i].parkRequest.Id,
                AllocationCompleted__c = true
            );
            if (i < availableSpaces) {
                prToUpdate.ParkAllocated__c = true;
            }
            updatedParkingRequests.add(prToUpdate);
        }
        update updatedParkingRequests;
    }

    private static ParkingSetting__mdt getParkingSettingMetadata() {
        return [
            SELECT Id, Label, AllocationTime__c, NumberOfSpaces__c, DayToAllocate__c
            FROM ParkingSetting__mdt
            WHERE DeveloperName = 'Auckland'
            LIMIT 1
        ];
    }

    private class ParkRequestWrapper implements Comparable {
        public ParkRequest__c parkRequest;
        public Integer numberOfRecentParks;

        ParkRequestWrapper(ParkRequest__c parkRequest, Integer numberOfRecentParks) {
            this.parkRequest = parkRequest;
            this.numberOfRecentParks = numberOfRecentParks;
        }

        public Integer compareTo(Object compareTo) {
            ParkRequestWrapper compareToPR = (ParkRequestWrapper)compareTo;

            if (this.parkRequest.SuperPark__c == compareToPR.parkRequest.SuperPark__c) {
                if (this.numberOfRecentParks > compareToPR.numberOfRecentParks) {
                    return 1;
                } else if (this.numberOfRecentParks < compareToPR.numberOfRecentParks) {
                    return -1;
                }
            } else if (this.parkRequest.SuperPark__c == false && compareToPR.parkRequest.SuperPark__c == true) {
                return 1;
            } else if (this.parkRequest.SuperPark__c == true && compareToPR.parkRequest.SuperPark__c == false) {
                return -1;
            }
            return Math.random() < 0.5 ? 1 : -1;
        }
    }

}