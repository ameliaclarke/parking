public without sharing class ParkingService {

    public static List<ParkRequestWrapper> getContactsParkingRequestRecords(Id contactId, Date requestDate){
        Date startOfWeek = requestDate.toStartOfWeek().addDays(1); // Monday start of week

        List<ParkRequest__c> existingParkRequests = [
            SELECT Id,
                     Name,
                     Date__c,
                     AllocationCompleted__c,
                     ParkAllocated__c,
                     Requested__c,
                     SuperPark__c,
                     Contact__c
            FROM ParkRequest__c
            WHERE Contact__c = :contactId
                AND Date__c >= :startOfWeek
                AND Date__c <= :startOfWeek.addDays(6)
        ];

        Map<Date, ParkRequest__c> existingParkRequestsMap = new Map<Date, ParkRequest__c>();
        for(ParkRequest__c pr : existingParkRequests) {
            existingParkRequestsMap.put(pr.Date__c, pr);
        }

        List<ParkRequest__c> thisWeeksParkRequests = new List<ParkRequest__c>();
        List<ParkRequestWrapper> thisWeeksParkRequestWrappers = new List<ParkRequestWrapper>();
        for (Integer i=0; i<5; i++) {
            Date prDate = startOfWeek.addDays(i);
            if (existingParkRequestsMap.containsKey(prDate)) {
                thisWeeksParkRequestWrappers.add(new ParkRequestWrapper(existingParkRequestsMap.get(prDate)));
            } else {
                thisWeeksParkRequestWrappers.add(
                    new ParkRequestWrapper(
                        new ParkRequest__c(
                            Contact__c = contactId,
                            Date__c = prDate
                        )

                    )
                );
            }
        }
        return thisWeeksParkRequestWrappers;
    }


    public static void saveContactsParkingRequestRecords(List<ParkRequestWrapper> parkRequestWrappers) {
        System.debug('*** ParkingService - parkRequestWrappers: ' + JSON.serializePretty(parkRequestWrappers));
        List<SObject> sObjectsToInsert = new List<SObject>();

        for (ParkRequestWrapper prw : parkRequestWrappers) {
            if (prw.parkRequest.Id == null) {
                sObjectsToInsert.add(prw.parkRequest);
            } else {
                if (prw.requested != prw.parkRequest.Requested__c || prw.superPark != prw.parkRequest.SuperPark__c) {
                    sObjectsToInsert.add(
                        new ParkRequestUpdate__c(
                            ParkRequest__c = prw.parkRequest.Id,
                            Requested__c = prw.requested,
                            SuperPark__c = prw.superPark,
                            ParkAllocated__c = prw.parkRequest.ParkAllocated__c
                        )
                    );
                }
            }
        }

        insert sObjectsToInsert;
    }

    public static void updatePark(Id contactId, Date requestDate, Boolean isParking) {
        List<ParkRequest__c> existingRequest = [
            SELECT Id, SuperPark__c
            FROM ParkRequest__c
            WHERE Contact__c = :contactId
                AND Date__c = :requestDate
        ];

        if (!existingRequest.isEmpty()) {
            insert new ParkRequestUpdate__c(
                ParkRequest__c = existingRequest[0].Id,
                SuperPark__c = existingRequest[0].SuperPark__c,
                Requested__c = isParking,
                ParkAllocated__c = isParking
            );
        } else {
            insert new ParkRequest__c(
                Contact__c = contactId,
                Date__c = requestDate,
                Requested__c = isParking,
                ParkAllocated__c = isParking
            );
        }
    }



    public class ParkRequestWrapper {
        public Boolean superPark {get; set;}
        public Boolean requested {get; set;}
        public ParkRequest__c parkRequest {get; set;}

        private ParkRequestUpdate__c parkRequestUpdate;

        public ParkRequestWrapper(ParkRequest__c parkRequest) {
            this.parkRequest = parkRequest;
            this.superPark = parkRequest.SuperPark__c;
            this.requested = parkRequest.Requested__c;
        }
    }

}
