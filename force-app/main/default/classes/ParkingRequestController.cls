public with sharing class ParkingRequestController {
    @AuraEnabled(cacheable=true)
    public static List<ParkRequest__c> getParkingRequests() {
        Date startOfWeek = Date.today().toStartOfWeek().addDays(1); // Monday start of week

        List<ParkRequest__c> existingParkRequests = [
            SELECT Id, Name, Date__c, ParkAllocated__c, Requested__c, SuperPark__c
            FROM ParkRequest__c
            WHERE  Date__c > :startOfWeek
                AND Date__c >= :startOfWeek.addDays(7)
        ];

        Map<Date, ParkRequest__c> existingParkRequestsMap = new Map<Date, ParkRequest__c>();
        for(ParkRequest__c pr : existingParkRequests) {
            existingParkRequestsMap.put(pr.Date__c, pr);
        }

        List<ParkRequest__c> parkRequests = new List<ParkRequest__c>();
        for (Integer i=0; i<5; i++) {
            Date prDate = startOfWeek.addDays(i);
            if (existingParkRequestsMap.containsKey(prDate)) {
                parkRequests.add(existingParkRequestsMap.get(prDate));
            } else {
                parkRequests.add(
                    new ParkRequest__c(
                        // User__c = UserInfo.getUserId(),
                        Date__c = prDate
                    )
                );
            }
        }
        System.debug('parkRequests: ' + parkRequests);
        // upsert parkRequests;
        return parkRequests;
    }

     @AuraEnabled(cacheable=true)
     public static void saveParkingRequests(List<ParkRequest__c> parkingRequests) {

    //  public static void saveParkingRequests(List<ParkRequest__c> parkingRequests) {
        System.debug('saveParkingRequests');
        System.debug('saveParkingRequests' + parkingRequests);

        // System.debug('parkingRequests' +parkingRequests);
     }

}