/**
 * A Hashed Id is an Id + a salt hashed using SHA-256. The String version is
 * a 64chr String.
 *
 * @author Ed (Trineo)
 * @date  21st July 2014
 */
public with sharing class HashedId {

    // Custom exception
    public class HashedIdException extends Exception {}

    private static String fixedSalt = 'FnTra';
    private String hashVal;

    public HashedId(String id) {
        this.hashVal = genHash(id, fixedSalt);
    }

    private static String genHash(String id, String salt) {
        if (id == null) {
                return null;
        }
        Blob b = Crypto.generateDigest('SHA-256', Blob.valueOf(salt + id));
        return EncodingUtil.convertToHex(b);
    }

    public static String createNewHash(String id) {
        return genHash(id, fixedSalt);
    }

    public static String createNewHash(String id, String customSalt) {
        // If custom salt is not specified, throw an error
        if(String.isBlank(customSalt)) {
            throw new HashedIdException('Custom salt required for creating new hash.');
        }
        return genHash(id, customSalt);
    }

    /**
     * Return a 64chr hex version of the SHA-256 version of the Id.
     */
    public override String toString() {
        return hashVal;
    }
}