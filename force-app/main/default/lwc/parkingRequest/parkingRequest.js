/* eslint-disable no-console */
import { LightningElement, wire, api, track } from 'lwc';
import getParkingRequests from '@salesforce/apex/ParkingRequestController.getParkingRequests';
import saveParkingRequests from '@salesforce/apex/ParkingRequestController.saveParkingRequests';

import { createRecord } from 'lightning/uiRecordApi';



export default class ParkingRequest extends LightningElement {
    // @wire(getParkingRequests) parkingRequests;


    // @api objectApiName = 'Contact';
    // @track data;
    // @track columns;

    // @wire(getParkingRequests)
    // wiredContacts({ data }) {
    //     console.log('*** wiredContacts');
    //     if (data) {
    //         this.data = data;
    //         //   this.columns = data.ldwList;
    //     }
    // }

    @track parkingRequests;

    @wire(getParkingRequests)
    wiredContacts({ error, data }) {
        if (data) {
            this.parkingRequests = data;
        } else if (error) {
            // this.error = error;
            this.parkingRequests = undefined;
        }
    }

    saveRecords() {
        saveParkingRequests({ wrapper: this.parkingRequests })
            .then(result => {
                console.log('*** huh1?');
                // this.contacts = result;
                // this.error = undefined;
            })
            .catch(error => {
                console.log('*** doh', error);
                // this.error = error;
                // this.contacts = undefined;
            });
    }
}