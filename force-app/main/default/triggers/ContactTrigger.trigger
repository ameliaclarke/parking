trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (Trigger.isInsert) {
        if (Trigger.isAfter) {
            populateUsernameHash();
        }
    }

    private static void populateUsernameHash() {
        List<Contact> contacts = new List<Contact>();
        for (Contact u : Trigger.new) {
            if (String.isBlank(u.Hash__c)) {
                String hashedUsername = new HashedId(u.Id).toString();
                contacts.add(
                    new Contact(
                        Id = u.Id,
                        Hash__c = hashedUsername
                    )
                );
            }
        }
        update contacts;
    }
}